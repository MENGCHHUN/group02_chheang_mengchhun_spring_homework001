package com.example.hwrestapi;

public class CustomerTemp {
    private String name;
    private String gender;
    private int age;
    private String address;

    public String getName() {
        return name;
    }
    public String getGender() {
        return gender;
    }
    public int getAge() {
        return age;
    }
    public String getAddress() {
        return address;
    }

}
