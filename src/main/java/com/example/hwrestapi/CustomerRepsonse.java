package com.example.hwrestapi;

public class CustomerRepsonse<T> {
    private String message;
    private T customers;
    private String status;
    private String dateTime;

    public CustomerRepsonse(String message, T customers, int status, String dateTime) {
        this.message = message;
        this.customers = customers;
        this.status = String.valueOf(status);
        this.dateTime = dateTime;
    }
    public CustomerRepsonse(String message, T customers , String status, String dateTime) {
        this.message = message;
        this.customers = customers;
        this.status = status;
        this.dateTime = dateTime;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomers() {
        return customers;
    }

    public void setCustomers(T customers) {
        this.customers = customers;
    }
}
