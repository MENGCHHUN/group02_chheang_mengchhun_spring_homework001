package com.example.hwrestapi;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CustomerController {
    // =================  CURRENT DATE TIME F0RMATED =====================
    LocalDateTime dateTime = LocalDateTime.now();
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    String formatDateTime = dateTime.format(format);
    // -------------------------------------------------------------------

    // ======================= VARIABLE & LIST =========================
    int id = 1;
    List<Customer> customers = new ArrayList<>();

    // ----------------------------------------------------------------

    // ====================== CONSTRUCTOR ====================
    public  CustomerController(){
        customers.add(new Customer(id++,"Chhun", "M",21, "Phnom Penh" ));
        customers.add(new Customer(id++,"Ty", "M",20, "Battambong" ));
        customers.add(new Customer(id++,"Vanneat", "F",23, "Kompong Cham" ));
    }

    // --------------------------------------------------------

    // ================= Get Customers ====================
    @GetMapping("api/v1/customers")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(
                new CustomerRepsonse<>(
                        "You got all record Succefully",
                        customers,
                        200,
                        formatDateTime
                )
        );
    }
    // ---------------------------------------------------

    // ================= Set Customer ====================
    @PostMapping("api/v1/customers")
    public ResponseEntity<?> setCustomer(@RequestBody CustomerTemp customerTemp){
        Customer newCustomer = new Customer();
        newCustomer.setId(id++);
        newCustomer.setName(customerTemp.getName());
        newCustomer.setGender(customerTemp.getGender());
        newCustomer.setAge(customerTemp.getAge());
        newCustomer.setAddress(customerTemp.getAddress());
        customers.add(newCustomer);
        return ResponseEntity.ok(
                new CustomerRepsonse<>(
                        "You're Insert record Successfully",
                        newCustomer,
                        200,
                        formatDateTime
                )
        );
    }
    // ------------------------------------------------------

    // =================== Get Customer by ID =================
    @GetMapping("api/v1/customers/{customerId}")
    public ResponseEntity<?>  getCustomerById (@PathVariable Integer customerId, HttpServletResponse res ){
        for (Customer customer: customers) {
            if (customer.getId() == customerId){
                return ResponseEntity.ok(
                    new CustomerRepsonse<>(
                            "This record has found successfully",
                            customer,
                            200,
                            formatDateTime
                    )
                );
            }else {
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        return null;
    }

    // ---------------------------------------------------------

    // =================== Get Customer by Name =================
    @GetMapping("api/v1/customers/search")
    @ResponseBody
    public ResponseEntity<?>  getCustomerByName ( @RequestParam  String customerName, HttpServletResponse res ){
        for (Customer customer: customers) {
            if (customer.getName().equals(customerName)) {
                return ResponseEntity.ok(
                   new CustomerRepsonse<>(
                       "This record has found successfully",
                        customer,
                        200,
                        formatDateTime
                   )
                );
            }else {
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        return null;
    }

    // -------------------------------------------------------------

    // =================== Delete Customer  by Id ==================
    @DeleteMapping("api/v1/costomers/delete/{customerID}")
    public  ResponseEntity<?>  deleteCustomerById(@PathVariable Integer customerID,HttpServletResponse res){
        for (Customer customer : customers){
           if(customer.getId() == customerID){
               customers.remove(new Customer().getId());
               return ResponseEntity.ok(
                  new CustomerRepsonse<>(
                     "Congratulation !! record has deleted successfully",
                     customer.getId(),
                     "Ok",
                     formatDateTime
                  )
               );
           }else {
               res.setStatus(HttpServletResponse.SC_NOT_FOUND);
           }
        }
        return null;
    }

    // -------------------------------------------------------------

    // =================== Update Customer by Id  ==================
    @PutMapping("api/v1/costomers/update/{customerID}")
    public ResponseEntity<?> updateCustomerById(@RequestBody CustomerTemp customerTemp, @PathVariable Integer customerID, HttpServletResponse res ){
        Customer newCustomer = new Customer();
        for (Customer customer1 : customers) {
            if(customer1.getId() == customerID){
                customers.remove(new Customer().getId());
                newCustomer.setId(customer1.getId());
                newCustomer.setName(customerTemp.getName());
                newCustomer.setGender(customerTemp.getGender());
                newCustomer.setAge(customerTemp.getAge());
                newCustomer.setAddress(customerTemp.getAddress());
                    customers.add(newCustomer);
                    return ResponseEntity.ok(
                          new CustomerRepsonse<>(
                          "You're record update successfully",
                          newCustomer,
                          200,
                          formatDateTime
                       )
                    );
            }else {
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        return null;
    }
    // -------------------------------------------------------------





}
