package com.example.hwrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class HwRestapiApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(HwRestapiApplication.class);
        app.setDefaultProperties(Collections.singletonMap("server.port", "8989"));
        app.run(args);
    }

}
